export function uuid(){
	return crypto.randomUUID();
}

export async function pipe( ...args ){
	var first = args.shift();

	if( typeof first == "function" ){
		first = first();
	}

	return await( args.reduce( async ( prev, next ) => {
		return next( await prev );
	}, first ) );
}

// Message Utilities
export function transaction( message = {} ){
	var tx = message.tx ? message.tx : uuid();

	return { tx };
}