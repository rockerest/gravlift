# gravlift

gravlift is a web application message courier.

- Decouple application parts
- Implement a standardized, common language for communication
- Expose internal APIs via reactive messaging instead of imperative coupling

## Example

```js
import { create } from "./gravlift.js";

var gravlift = create();

gravlift.subscribeWith( {
    "my::cool::custom-event": ( shuttle ) => {
        let node = document.createElement( "div" );

        node.textContent = shuttle.message;

        document.getElementById( "global-alert-box" ).appendChild( node );
    }
} );

// Later, in an application sub-module far, far away
afterRender(){
    var gravlift = this.dependencyInjection.gravlift;

    document
        .getElementById( "our-very-critical-button" )
        .addEventListener( "click", () => {
            gravlift.publish( {
                "name": "my::cool::custom-event", 
                "message": "Thank you for clicking our Very Important Button!"
            } );
        } );
}
```

An application can define a catalogue of named events that can occur, and those define the API boundaries of various sections of an application.  
These standardized, common events are also multicast, which means - should another section of the application ever care about the click of the Very Important Button - the triggering location does not need to be modified in any way. Only the new component that cares needs to listen for the existing event, and react to it.