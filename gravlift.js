import { transaction, uuid, pipe } from "./utilities.js";

function getUnhandledFallbackListeners( listeners ){
	return listeners[ "*" ] || [];
}

function getEveryListeners( listeners ){
	return listeners[ "**" ] || [];
}

function getEventNameFromMessage( message ){
	return typeof message == "string" ? message : message.name;
}

function getListenersForEventName( listeners, name ){
	var base = listeners[ name ] ? listeners[ name ] : [ ...getUnhandledFallbackListeners( listeners ) ];

	return [ ...base, ...getEveryListeners( listeners ) ];
}

function getHandlersForListeners( handlers, listenerIds ){
	return listenerIds.map( ( id ) => handlers[ id ] );
}

function getUnsubscribers( unsubscribe = {} ){
	let normalizedUnsubscribers = {};

	if( unsubscribe instanceof Array ){
		unsubscribe = {
			"before": unsubscribe
		};
	}

	return Object.assign(
		normalizedUnsubscribers,
		{
			"before": [],
			"after": []
		},
		unsubscribe
	);
}

function getTx( txOrMessage, previousTx ){
	let tx = previousTx;

	if( txOrMessage?.name ){
		tx = txOrMessage.tx;
	}
	else if( typeof txOrMessage == "string" ){
		tx = txOrMessage;
	}

	return tx;
}

function *processor(){
	var queue = [];
	var subscribers = [];
	var first;

	while( true ){
		while( queue.length ){
			first = queue.shift();

			subscribers.forEach( ( subscriber ) => subscriber( first ) ); /* eslint-disable-line no-loop-func */
		}

		( { queue, subscribers } = yield );
	}
}

function createGeneratorBus(){
	var active = true;
	var queue = [];
	var subscribers = [];
	var stream = processor();
	var bus = {
		subscribe( subscriber ){
			subscribers.push( subscriber );
		},
		next( ...input ){
			queue.push( ...input );
			bus.drain();
		},
		drain(){
			if( active ){
				stream.next( { queue, subscribers } );
			}
		},
		activate(){
			active = true;
		},
		deactivate(){
			active = false;
		},
		start(){
			bus.activate();
			bus.drain();
		},
		stop(){
			bus.deactivate();
		}
	};

	// Hot start
	bus.start();

	return bus;
}

function attachMethods( busInstance ){
	var publicInterface = {
		publish( msg ){
			var newMessage;

			if( typeof msg == "string" ){
				newMessage = {
					...transaction(),
					"name": msg
				};
			}
			else{
				newMessage = { ...transaction( msg ), ...msg };
			}

			this.stream.next( newMessage );

			return newMessage;
		},
		subscribeWith( map ){
			var names = Object.keys( map );
			var handlers = Object.values( map );
			var idMap = [];
	
			handlers.forEach( ( handler, i ) => {
				let name = names[ i ];
				let id = uuid();
	
				this.handlers[ id ] = handler;
				if( this.listeners[ name ] ){
					this.listeners[ name ].push( id );
				}
				else{
					this.listeners[ name ] = [ id ];
				}
				idMap.push( { name, id } );
			} );
	
			return {
				unsubscribe(){
					idMap.forEach( ( { name, id } ) => {
						delete busInstance.handlers[ id ];
	
						if( busInstance.listeners[ name ] ){
							busInstance.listeners[ name ] = busInstance.listeners[ name ].filter( ( listener ) => listener != id );
						}
	
						if( busInstance.listeners[ name ].length == 0 ){
							delete busInstance.listeners[ name ];
						}
					} );
				}
			};
		},
		once( messageName, handler ){
			var stream;
			var map = {
				[messageName]: ( ...args ) => {
					handler( ...args );
	
					stream.unsubscribe();
				}
			};
	
			stream = this.subscribeWith( map );
	
			return stream;
		},
		sequence( sequenceConfiguration ){
			var bus = this;
			var { initial, stages } = sequenceConfiguration;
			var listeners = {};
			var tx;
	
			return {
				unsubscribe(){
					Object
						.values( listeners )
						.forEach( ( { unsubscribe } ) => unsubscribe() );
				},
				trigger(){
					Object
						.entries( stages )
						.forEach( ( [ stageName, stageConfiguration ] ) => {
							let { name, handler, unsubscribe } = stageConfiguration;
							let { before, after } = getUnsubscribers( unsubscribe );
	
							listeners[ stageName ] = bus.once( name, ( message ) => {
								if( message.tx == tx ){
									before.forEach( ( unsub ) => {
										listeners[ unsub ]?.unsubscribe();
									} );
	
									if( handler ){
										tx = getTx( handler( message ), tx );
									}
	
									after.forEach( ( unsub ) => {
										listeners[ unsub ]?.unsubscribe();
									} );
								}
							} );
						} );
	
					tx = bus.publish( initial ).tx;
				}
			};
		}
	};

	Object
		.entries( publicInterface )
		.forEach( ( [ name, implementation ] ) => {
			busInstance[ name ] = implementation.bind( busInstance );
		} );
}

export function Gravlift( settings = {}, ...args ){
	var self = this;

	if( !( this instanceof Gravlift ) ){
		self = new Gravlift( settings, ...args );
	}
	else{
		this.listeners = {};
		this.handlers = {};
		this.stream = createGeneratorBus();

		this.stream.subscribe( ( event ) => {
			pipe(
				event,
				getEventNameFromMessage,
				( name ) => getListenersForEventName( this.listeners, name ),
				( listeners ) => getHandlersForListeners( this.handlers, listeners ),
				( handlers ) => {
					handlers.forEach( ( handler ) => {
						handler( event );
					} );
				}
			);
		} );

		attachMethods( this );
	}

	return self;
}

export function create( ...args ){
	return new Gravlift( ...args );
}